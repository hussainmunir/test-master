package com.tuts.prakash.simpleocr;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    SurfaceView mCameraView;
    TextView mTextView;
    CameraSource mCameraSource;
    Camera camera;
    private boolean cameraFront = false;
    ImageButton captureImageButton;
    ImageButton rotateImageButton;
    ImageButton flashImageButton;
    ArrayList<String> fileData = new ArrayList<>();

    private static final String TAG = "MainActivity";
    private static final int requestPermissionID = 101;
    private Uri imageuri;
    private static final String SAVED_INSTANCE_URI = "uri";
    private boolean flashmode = false;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private int cameraId;
    private int rotation;
    private Paint mPaint;
    private Path mPath;
     Canvas canvas;
     SurfaceHolder mSurfaceHolder;
    private SurfaceHolder holderTransparent;
    private float mX, mY, newX, newY;


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCameraView = findViewById(R.id.surfaceView);
        mTextView = findViewById(R.id.text_view_suggestion_id);
        captureImageButton = (ImageButton) findViewById(R.id.captureImageButtonId);
        rotateImageButton = (ImageButton)findViewById(R.id.rotateCameraId);
        flashImageButton = (ImageButton)findViewById(R.id.flashCameraId);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        cameraId = CameraSource.CAMERA_FACING_BACK;
        startCameraSource(cameraId);
//        openCamera(cameraId);
        readData();
      //  mSurfaceHolder = mCameraView.getHolder();
        canvas = new Canvas();


        captureImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                DrawRect(88,248,837,711, Color.RED);

            }
        });
        rotateImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
   /*             if (cameraFront) {

                    final TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
                    mCameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                            .setFacing(CameraSource.CAMERA_FACING_BACK)
                            .setRequestedPreviewSize(1280, 1024)
                            .setAutoFocusEnabled(true)
                            .setRequestedFps(2.0f)
                            .build();
                    cameraFront = false;
                }
                else {
                    final TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
                    mCameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                            .setFacing(CameraSource.CAMERA_FACING_FRONT)
                            .setRequestedPreviewSize(1280, 1024)
                            .setAutoFocusEnabled(true)
                            .setRequestedFps(2.0f)
                            .build();
                    cameraFront = true;
                }
*/

   /*             if(mCameraSource.getCameraFacing()==CameraSource.CAMERA_FACING_FRONT){
                    if (mCameraSource != null) {
                        mCameraSource.release();
                    }
                    startCameraSource(CameraSource.CAMERA_FACING_BACK);
                }
                else{
                    if (mCameraSource != null) {
                        mCameraSource.release();
                    }
                    startCameraSource(CameraSource.CAMERA_FACING_FRONT);
                }
*/
            }
        });
        flashImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                flashOnButton();
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != requestPermissionID) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mCameraSource.start(mCameraView.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void startCameraSource(int cameraFacing) {

        //Create the TextRecognizer
        final TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();


        if (!textRecognizer.isOperational()) {
            Log.w(TAG, "Detector dependencies not loaded yet");
        } else {

            //Initialize camerasource to use high resolution and set Autofocus on.
            mCameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(cameraFacing)
                    .setRequestedPreviewSize(1280, 1024)
                    .setAutoFocusEnabled(true)
                    .setRequestedFps(2.0f)
                    .build();

            /**
             * Add call back to SurfaceView and check if camera permission is granted.
             * If permission is granted we can start our cameraSource and pass it to surfaceView
            */
            mCameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {

                        mSurfaceHolder = holder;


//                            camera = Camera.open();

                           // camera.setPreviewDisplay(holder);

//                            camera.startPreview();


                        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    requestPermissionID);
                            return;
                        }
                        mCameraSource.start(holder);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }




                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                }


                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    mCameraSource.stop();
                }
            });
            //Set the TextRecognizer's Processor.
            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {
                }

                /**
                 * Detect all the text from camera using TextBlock and the values into a stringBuilder
                 * which will then be set to the textView.
                 * */
                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections)
                {

                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                     final ArrayList<String> resultsArray = new ArrayList<>();

                    if (items.size() != 0 ){

                        mTextView.post(new Runnable() {
                            @Override
                            public void run() {
                                StringBuilder stringBuilder = new StringBuilder();
                                for(int i=0;i<items.size();i++) {
                                    TextBlock item = items.valueAt(i);
                                    item.getBoundingBox();
//                                    stringBuilder.append(item.getValue());
//                                    stringBuilder.append("\n");
//                                    mTextView.setText(item.getValue());


                                    String[] multiItems = item.getValue().split("\n");
//                                    if (item.getValue().matches("SHAHEEN GROCER'S")){
//                                        stringBuilder.append("Store is Found");
//                                        stringBuilder.append("\n");
//                                    }
//                                    if (item.getValue().matches("Sub Total")){
//                                        stringBuilder.append("Total is Found");
//                                        stringBuilder.append("\n");
//                                    }
//                                    String unitString = "";
//                                    if (i < fileData.size()) {
//                                        unitString = fileData.get(i);
//                                    }
//                                    if(item.getValue().matches(fileData.get(i)))
//                                    {
//                                        resultsArray.add(unitString);
//                                        //notifiy adapter
//                                    }
                                    for (String com : multiItems)
                                    {
                                        if (fileData.contains(com.toLowerCase())) {
                                            resultsArray.addAll(Arrays.asList(multiItems));
                                            //notifiy adapter
                                            stringBuilder.append(com);
                                            stringBuilder.append("\n");

                                            try {

                                                canvas = mSurfaceHolder.lockCanvas();
                                                synchronized(mSurfaceHolder) {
//                                onDraw(canvas);
                                                    DrawRect(item.getBoundingBox().left,item.getBoundingBox().top,item.getBoundingBox().right,item.getBoundingBox().bottom, Color.RED);

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            } finally {
                                                if (canvas != null) {
                                                    mSurfaceHolder.unlockCanvasAndPost(canvas);
                                                }
                                            }
//                                            DrawRect(item.getBoundingBox().left,item.getBoundingBox().top,item.getBoundingBox().right,item.getBoundingBox().bottom, Color.RED);
                                        }
                                    }
                                    if (resultsArray.size() > 0) {
                                        //mTextView.setText(stringBuilder.toString());
                                        captureImageButton.setBackground(getResources().getDrawable(R.mipmap.shutterbuttonon));
                                    }


//                                    Bitmap bitmap = null;
//                                    GraphicOverlay graphicOverlay = new GraphicOverlay(MainActivity.this);
//                                    CameraImageGraphic imageGraphic = new CameraImageGraphic(graphicOverlay,
//                                           bitmap );
//                                    graphicOverlay.add(imageGraphic);
//                                    GraphicOverlay.Graphic textGraphic = new TextGraphic(graphicOverlay,items.get(i));
//                                    graphicOverlay.add(textGraphic);

                                }



                          /* for(int i=0; i<items.size(); i++)
                                    {
                                        String unitString = fileData.get(i);
                                        if(unitString.contains(items.get(i).toString())){
                                            resultsArray.add(unitString);
                                            //notifiy adapter
                                        }
                                    }*/

//                                mTextView.setText(stringBuilder.toString());

                            }
                        });
                    }
                    else
                    {
//                        mTextView.setText("No Text");
                    }


                }
            });
        }
    }
     int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;

    }

     int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;

            }

        }
        return cameraId;
    }

    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }



    private void readData() {
        InputStream is = getResources().openRawResource(R.raw.welldata);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8")));
        String line = "";

        try {
            while ((line = reader.readLine()) != null) {
                // Split the line into different tokens (using the comma as a separator).
                String[] tokens = line.split(",");

                // Read the data and store it in the WellData POJO.
//                wellData.setOwner(tokens[0]);
//                wellData.setLongitude(tokens[2]);
//                wellData.setLatitude(tokens[3]);
//                wellData.setProperty(tokens[4]);
//                wellData.setWellName(tokens[5]);
//                wellDataList.add(wellData);


                fileData.add(tokens[0].toLowerCase());



                Log.d("MainActivity" ,"Just Created "+ tokens[0]);
            }
        } catch (IOException e1) {
            Log.e("MainActivity", "Error" + line, e1);
            e1.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.setErrorCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(camera != null){
            camera.setPreviewCallback(null);
            camera.setErrorCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }


    private boolean openCamera(int id) {
        boolean result = false;
        cameraId = id;
        //releaseCamera();
        try {
            camera = Camera.open(cameraId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (camera != null) {
            try {
                setUpCamera(camera);
                camera.setErrorCallback(new Camera.ErrorCallback() {

                    @Override
                    public void onError(int error, Camera camera) {

                    }
                });
                camera.setPreviewDisplay(mCameraView.getHolder());
                camera.startPreview();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
                releaseCamera();
            }
        }
        return result;
    }
    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.setErrorCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", e.toString());
            camera = null;
        }
    }

    private void releaseCameraSource() {
        try {
            if (mCameraSource != null) {
                mCameraSource.stop();
                mCameraSource.release();
                mCameraSource = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", e.toString());
            mCameraSource = null;
        }
    }

    private void setUpCamera(Camera c) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degree = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degree = 0;
                break;
            case Surface.ROTATION_90:
                degree = 90;
                break;
            case Surface.ROTATION_180:
                degree = 180;
                break;
            case Surface.ROTATION_270:
                degree = 270;
                break;

            default:
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            rotation = (info.orientation + degree) % 330;
            rotation = (360 - rotation) % 360;
        } else {
            // Back-facing
            rotation = (info.orientation - degree + 360) % 360;
        }
        c.setDisplayOrientation(rotation);
        Parameters params = c.getParameters();


        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (focusModes
                    .contains(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFlashMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
        }

        params.setRotation(rotation);
    }


    private void flashOnButton() {

        if (camera != null) {
            try {
                Parameters param = camera.getParameters();
                if (flashmode) {
                    param.setFlashMode(Parameters.FLASH_MODE_OFF);
                    flashImageButton.setBackground(getResources().getDrawable(R.mipmap.flashoff));
                } else {
                    param.setFlashMode(Parameters.FLASH_MODE_TORCH);
                    flashImageButton.setBackground(getResources().getDrawable(R.mipmap.flashon));
                }
                //param.setFlashMode(!flashmode ? Parameters.FLASH_MODE_TORCH : Parameters.FLASH_MODE_OFF);
                camera.setParameters(param);
                flashmode = !flashmode;
            } catch (Exception e) {
                e.printStackTrace();
                // TODO: handle exception
            }

        }
    }

    private void DrawRect(float RectLeft, float RectTop, float RectRight, float RectBottom, int color)
    {
        mPath = new Path();
        mPath.moveTo(RectLeft, RectTop);

//        canvas = mSurfaceHolder.lockCanvas();
//        canvas.drawColor(0, Mode.CLEAR);
//        canvas.drawRGB(255, 0, 0);
        //border's properties
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(color);
        paint.setStrokeWidth(3);
        canvas.drawRect(RectLeft, RectTop, RectRight, RectBottom, paint);
        canvas.restore();

//        mSurfaceHolder.unlockCanvasAndPost(canvas);
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawRGB(255, 0, 255);
    }
}
